// import '@assets/custom-vars.scss'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  server: {
    host: '0.0.0.0' // default: localhost
  },

  loading: {
    color: '#00c58e',
    height: '10px'
  },

  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Customize the generated output folder
  generate: {
    dir: 'public'
  },

  router: {
    base
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Parking Meters',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'
      },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/custom-vars.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '@/plugins/infinite-scroll.js', ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      'nuxt-gmaps',
      {
        key: 'AIzaSyC96mK8ueL2_6BmQDP-DZWb-nt22W7roGU'
        // you can use libraries: ['places']
      }
    ]
  ],
  bootstrapVue: {
    bootstrapCSS: true, // here you can disable automatic bootstrapCSS, in case you are loading your custom sass
    bootstrapVueCSS: true, // CSS that is specific to bootstrapVue components can also be disabled. That way you won't load css for modules that you don't use
    componentPlugins: [
      'OverlayPlugin',
      'AlertPlugin',
      'ButtonGroupPlugin',
      'ButtonPlugin',
      'CardPlugin',
      'FormSelectPlugin',
      'PaginationPlugin',
      'TablePlugin',
      'LinkPlugin'
    ],
    directivePlugins: []
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      name: 'Parking Meters',
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      compact: true
    }
  }
}
